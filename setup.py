# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of pytest-lint-ansible
#
# pytest-lint-ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pytest-lint-ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with pytest-lint-ansible.  If not, see <http://www.gnu.org/licenses/>.


from pathlib import Path

from setuptools import find_packages, setup


def get_requirements(path):
    lines = Path(path).open('r')
    lines = map(lambda l: l.strip(), lines)
    lines = filter(lambda l: bool(l), lines)
    lines = filter(lambda l: not l.startswith('#'), lines)

    return list(lines)


README = Path('README.md').read_text()
CHANGES = Path('CHANGES.md').read_text()
REQUIREMENTS = get_requirements('requirements.txt')


setup(
    name='pytest-lint-ansible',
    description='Verify your ansible roles with pytest',
    long_description='%s\n\n%s' % (README, CHANGES),
    version='0.1',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Plugins',
        'Framework :: Pytest',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        ('License :: OSI Approved :: GNU Affero General Public License v3 or '
         'later (AGPLv3+)'),
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Software Development :: Testing',
        'Topic :: System :: Systems Administration',
    ],
    author='Mathieu Bridon',
    author_email='bochecha@daitauha.fr',
    url='https://framagit.org/bochecha/pytest-lint-ansible/',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=REQUIREMENTS,
    entry_points={
        'pytest11': [
            'pytest-lint-ansible = pytest_lint_ansible',
        ],
    },
)
