# pytest-lint-ansible

This [Pytest](https://pytest.org) plugin makes it a breeze to lint your
[Ansible](https://ansible.com) roles.

# Usage

Install the plugin:

    $ pip install pytest-lint-ansible

Then tell Pytest to use it:

    $ py.test --lint-ansible

# Configuration

## Roles directory

By default pytest-lint-ansible will assume that there is a `roles` directory
directly under the root of your project, containing your Ansible roles.

If your project uses a different layout, you can specify the `ansiblerolesdir`
option, for example in your `setup.cfg` file:

```ini
[tool:pytest]
ansiblerolesdir = roles
```

## Skipping some ansible-lint verifications

Among other things, pytest-lint-ansible runs
[ansible-lint](https://github.com/willthames/ansible-lint) on your roles, which
reports a lot of things as bad practices.

If you have good reasons to ignore some of ansible-lint's recommendations, you
can skip those tests using the `ansiblelintskip` option, for example:

```ini
[tool:pytest]
ansiblelintskip =
    ANSIBLE0002
    ANSIBLE0010
```

# Legalities

pytest-lint-ansible is offered under the terms of the
[GNU Affero General Public License, either version 3 or any later version](http://www.gnu.org/licenses/agpl.html).

We will never ask you to sign a copyright assignment or any other kind of
silly and tedious legal document before accepting your contributions.

In case you're wondering, we do **not** consider that using pytest-lint-ansible
makes your ansible roles and playbooks licensed under the AGPL.
