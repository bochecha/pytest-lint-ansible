# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of pytest-lint-ansible
#
# pytest-lint-ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pytest-lint-ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with pytest-lint-ansible.  If not, see <http://www.gnu.org/licenses/>.


import os
import pathlib
import tempfile


class NotARole(Exception):
    pass


# FIXME: I'd love to see something like this in Python
class TemporaryDirectory(tempfile.TemporaryDirectory):
    def __enter__(self):
        return pathlib.Path(self.name)


def get_role_and_path(config, path):
    relpath = path.relto(config._ansiblerolesdir)

    if not relpath:
        raise NotARole(path)

    role = relpath.split(os.path.sep)[0]
    rolepath = config._ansiblerolesdir.join(role)

    return role, rolepath
