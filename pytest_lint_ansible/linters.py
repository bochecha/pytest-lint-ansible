# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of pytest-lint-ansible
#
# pytest-lint-ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pytest-lint-ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with pytest-lint-ansible.  If not, see <http://www.gnu.org/licenses/>.


import os
from subprocess import PIPE, STDOUT, Popen

from .utils import TemporaryDirectory


class LinterError(Exception):
    pass


def ansible_lint(rolename, roles_path, skip_list):
    rolepath = os.path.join(roles_path, rolename)

    with TemporaryDirectory() as tmpdir:
        proc = Popen(
            ['ansible-lint', '-x', ','.join(skip_list), rolepath],
            stdout=PIPE, stderr=STDOUT, cwd=str(tmpdir),
            env={'PATH': os.environ['PATH']})
        out, _ = proc.communicate()

    if proc.returncode:
        raise LinterError(out.decode())


def ansible_playbook_syntax(rolename, roles_path):
    with TemporaryDirectory() as tmpdir:
        inventory = tmpdir.joinpath('inventory')
        inventory.write_text('localhost ansible_connection=local')

        config = tmpdir.joinpath('ansible.cfg')
        config.write_text(
            '[defaults]\n'
            'inventory = {inventory}\n'
            'roles_path = {roles_path}\n'.format(
                inventory=inventory, roles_path=roles_path))

        playbook = tmpdir.joinpath('playbook.yml')
        playbook.write_text(
            '---\n'
            '- hosts: localhost\n'
            '  roles:\n'
            '  - role: {rolename}'.format(rolename=rolename))

        proc = Popen(
            ['ansible-playbook', '--syntax-check', '-vvv', str(playbook)],
            stdout=PIPE, stderr=STDOUT, cwd=str(tmpdir),
            env={'ANSIBLE_CONFIG': str(config), 'PATH': os.environ['PATH']})
        out, _ = proc.communicate()

    if proc.returncode:
        raise LinterError(out.decode())
