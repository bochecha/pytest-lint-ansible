# Copyright (c) 2016 - Mathieu Bridon <bochecha@daitauha.fr>
#
# This file is part of pytest-lint-ansible
#
# pytest-lint-ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pytest-lint-ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with pytest-lint-ansible.  If not, see <http://www.gnu.org/licenses/>.


import py
import pytest

from .linters import ansible_playbook_syntax, ansible_lint, LinterError
from .utils import get_role_and_path, NotARole


class AnsibleCollector(pytest.collect.File):
    def __init__(self, fspath, role, parent=None):
        super().__init__(fspath, parent=parent)
        self.role = role

    def collect(self):
        return (
            AnsiblePlaybookSyntaxItem(self.role, parent=self),
            AnsibleLintItem(self.role, parent=self),
        )


class AnsibleItem(pytest.collect.Item):
    def __init__(self, role, parent=None):
        super().__init__(self.__basename__ % role, parent=parent)
        self.role = role

    def repr_failure(self, excinfo):
        if excinfo.errisinstance(LinterError):
            return excinfo.value.args[0]

        return super().repr_failure(excinfo)

    def reportinfo(self):
        return (self.fspath, None, self.name)


class AnsibleLintItem(AnsibleItem):
    __basename__ = 'ansible-lint %s'

    def runtest(self):
        rolesdir = str(self.config._ansiblerolesdir)
        skiplist = self.config._ansiblelintskip

        ansible_lint(self.role, rolesdir, skiplist)


class AnsiblePlaybookSyntaxItem(AnsibleItem):
    __basename__ = 'ansible-playbook --syntax %s'

    def runtest(self):
        rolesdir = str(self.config._ansiblerolesdir)

        ansible_playbook_syntax(self.role, rolesdir)


def pytest_addoption(parser):
    group = parser.getgroup('general')
    group.addoption(
        '--lint-ansible', action='store_true',
        help='Verify your ansible roles')
    parser.addini(
        'ansiblerolesdir', default='roles',
        help='the directory containing your ansible roles, relative to the '
             'project root')
    parser.addini(
        'ansiblelintskip', type='linelist', default=[],
        help='the list of ansible-lint checks to skip')


def pytest_sessionstart(session):
    config = session.config

    if config.option.lint_ansible:
        cwd = py.path.local()
        config._ansiblerolesdir = cwd.join(config.getini('ansiblerolesdir'))
        config._ansiblelintskip = config.getini('ansiblelintskip')


def pytest_collect_file(path, parent):
    config = parent.config

    if not config.option.lint_ansible:
        return

    try:
        role, rolepath = get_role_and_path(config, path)

    except NotARole:
        return

    # TODO: Is this the best way? pytest_collect_directory would have been nice
    if not hasattr(parent, '_collected_roles'):
        parent._collected_roles = []

    if role not in parent._collected_roles:
        parent._collected_roles.append(role)
        return AnsibleCollector(rolepath, role, parent=parent)
